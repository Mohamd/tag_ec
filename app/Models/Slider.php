<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Slider
 * @package App\Models
 * @version January 23, 2020, 6:33 am UTC
 *
 * @property string text
 * @property string image
 */
class Slider extends Model
{

    public $table = 'sliders';
    public $timestamps= false;



    public $fillable = [
        'text',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'text' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
