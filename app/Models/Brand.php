<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Brand
 * @package App\Models
 * @version January 21, 2020, 9:31 am UTC
 *
 * @property string name
 * @property string image
 * @property string des
 */
class Brand extends Model
{

    public $table = 'brands';
    



    public $fillable = [
        'name',
        'image',
        'des'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'image' => 'string',
        'des' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
    public function category()
    {
        return $this->belongsToMany(\App\Models\Category::class, 'brand_category', 'brand_id', 'category_id');
    }
    
    public function product(){
        return $this->hasMany('App\Models\Product','brand_id','id');
    }
}
