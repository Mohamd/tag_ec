<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Category
 * @package App\Models
 * @version January 21, 2020, 10:02 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection brands
 * @property string name
 * @property string des
 * @property string image
 */
class Category extends Model
{

    public $table = 'categories';
    public $timestamps= false;



    public $fillable = [
        'name',
        'des',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'des' => 'string',
        'image' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     **/
  
    public function product(){
        return $this->hasMany(\App\Models\Product::class,'category_id','id');
    }
     public function brands()
    {
        return $this->belongsToMany(\App\Models\Brand::class, 'brand_category', 'category_id', 'brand_id');
    }
}
