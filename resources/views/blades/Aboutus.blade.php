@extends('blades.master')
@section('content')


<!-- Page Content -->
       <main class="page-content">



<!-- Aboout Area -->
<div class="tm-about-area tm-padding-section bg-white">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="tm-about-image">
                    <img src="{{asset('/images/about-image.jpg')}}" alt="about image">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="tm-about-content">
                    <h4>{{$abuttitle[0]->value}}</h4>

                    <p>{{$aboutdes[0]->value}}</p>

                </div>
            </div>
        </div>
    </div>
</div>
<!--// Aboout Area -->





</main>
<!--// Page Content -->
@stop
