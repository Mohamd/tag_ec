<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Des Field -->
<div class="form-group col-sm-6">
    {!! Form::label('des', 'Des:') !!}
    {!! Form::text('des', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image') !!}
</div>
<div class="clearfix"></div>

<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', 'Category:') !!}
  
    {!! Form::select('category_id', ['category' => $category], null, ['class' => 'form-control']) !!}
    
</div>

<!-- price  Field -->
<div class="form-group col-sm-6">
    {!! Form::label('price', 'Price:') !!}
  
<input type="number" name="price" class="form-control" value="{{$product->price}}" >   
</div>

<!-- offer price  Field -->
<div class="form-group col-sm-6">
    {!! Form::label('offer price', 'Offer Price:') !!}
  
<input type="number" name="offer_price" class="form-control" value="{{$product->offer_price}}" >   
</div>

<!-- on sale Field -->
<div class="form-group col-sm-6">
   <label>On Sale </label>
    <input type="radio"  name="on_sale" value="1" required="required"  <?php if($product->on_sale==1){echo"checked";} ?> /> yes
                        <input type="radio" name="on_sale" value="0" required="required"  <?php if($product->on_sale==0){echo"checked";}?> />No
</div>

<!-- home Field -->
<div class="form-group col-sm-6">
   <label>Home Page  </label>
    <input type="radio"  name="home" value="1" required="required" <?php if($product->home==1){echo"checked";} ?> /> yes
                        <input type="radio" name="home" value="0" required="required" <?php if($product->home==0){echo"checked";} ?> />No
</div>


<!-- deal Field -->
<div class="form-group col-sm-6">
   <label>Deal</label>
    <input type="radio"  name="deal" value="1" <?php if($product->deal==1){echo"checked";} ?>  /> yes
    <input type="radio" name="deal" value="0"  <?php if($product->deal==0){echo"checked";} ?> />No
</div>

<!-- deal date -->
<div class="form-group col-sm-6">
   <label>Deal date</label>
    <input type="date"  name="deal_date" value="{{$product->deal_date}}" /> 
   
</div>

<!-- deal price  Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deal price', 'Deal Price:') !!}
  
<input type="number" name="deal_price" class="form-control" value="{{$product->deal_price}} >   
</div>


<!-- quantity  Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Quantity', 'Quantity:') !!}
  
<input type="number" name="quantity" class="form-control" value="{{$product->quantity}}"  required >   
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('products.index') }}" class="btn btn-secondary">Cancel</a>
</div>
